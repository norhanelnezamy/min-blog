<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Category extends Model
{
    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [
        'name'
    ];

    /**
     * insert new category row.
     */
    public static function insertRow($data)
    {
        $category = new Category();
        $category->name = $data->name;
        $category->save();
    }

    /**
     * update category row.
     */
    public static function updateRow($data, $id)
    {
        $category = Category::findOrFail($id);
        $category->name = $data->name;
        $category->save();
    }

    /**
     * every category has many articles.
     */
    public function articles()
    {
        return $this->hasMany('App\Article');
    }
}
