<?php

namespace App\Http\Requests;

use Illuminate\Foundation\Http\FormRequest;

class UserRequest extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        $id = ($this->method()=="PUT")? $this->route('user'):'';
        return [
            'name'=>'required|between:3,200',
            'email'=>'required|email|unique:users,email,'.$id,
            'password'=> empty($id)?'required|between:6,12':'nullable|between:6,12',
            'type'=> 'required',
        ];
    }
}
