<?php

namespace App\Http\Controllers\Dashboard;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use App\Http\Requests\CategoryRequest;
use App\Category;

class CategoryController extends Controller
{
  /**
  * Display a listing of the resource.
  *
  * @return \Illuminate\Http\Response
  */
  public function index()
  {
    session()->put('side_bar', 'category_index');
    $data['categories'] = Category::orderBy('created_at', 'DESC')->get();
    return view('dashboard.category.index', $data);
  }

  /**
  * Show the form for creating a new resource.
  *
  * @return \Illuminate\Http\Response
  */
  public function create()
  {
    session()->put('side_bar', 'category_create');
    return view('dashboard.category.insert');
  }

  /**
  * filter a listing of the resource.
  *
  * @return \Illuminate\Http\Response
  */
  public function show($id)
  {
    //
  }

  /**
  * Store a newly created resource in storage.
  *
  * @param  \Illuminate\Http\Request  $request
  * @return \Illuminate\Http\Response
  */
  public function store(CategoryRequest $request)
  {
    Category::insertRow($request);
    return redirect()->back()->with('msg', 'add_msg');
  }

  /**
  * Show the form for editing the specified resource.
  *
  * @param  int  $id
  * @return \Illuminate\Http\Response
  */
  public function edit($id)
  {
    $data['category'] = Category::find($id);
    return view('dashboard.category.update', $data);
  }

  /**
  * Update the specified resource in storage.
  *
  * @param  \Illuminate\Http\Request  $request
  * @param  int  $id
  * @return \Illuminate\Http\Response
  */
  public function update(CategoryRequest $request, $id)
  {
    Category::updateRow($request, $id);
    return redirect()->back()->with('msg', 'update_msg');
  }

  /**
  * Remove the specified resource from storage.
  *
  * @param  int  $id
  * @return \Illuminate\Http\Response
  */
  public function destroy($id)
  {
    Category::findOrFail($id)->delete();
    return response()->json(['status' => 1, 'msg' => 'delete_msg']);
  }
}
