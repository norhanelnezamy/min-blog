<?php

namespace App\Http\Controllers\Dashboard;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;

class IndexController extends Controller
{
  public function index()
  {
    session()->put('side_bar', 'index');
    return view('dashboard.index');
  }

}
