<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Category;
use App\Article;

class IndexController extends Controller
{

    public function index()
    {
        $data['categories'] = Category::all();
        $data['articles'] = Article::with('comments')->paginate(30);
        return view('index', $data);
    }


    public function show($id)
    {
        $data['categories'] = Category::all();
        $data['articles'] = Article::where('category_id',$id)->with('comments')->paginate(30);
        return view('index', $data);
    }

}
