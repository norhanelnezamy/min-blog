<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Http\Requests\LoginRequest;

class LoginController extends Controller
{
    public function getLogin()
    {
        if (auth()->check()) {
            return redirect('/admin');
        }
        return view('login');
    }

    public function postLogin(LoginRequest $request)
    {
        if (auth()->attempt($request->only(['email', 'password']))) {
            return redirect('dashboard');
        }
        return redirect()->back()->with('msg', 'Invalid email or password .');
    }

    public function logout()
    {
        auth()->logout();
        return redirect('/');
    }
}
