<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Http\Requests\CommentRequest;
use App\Comment;
use Carbon;

class CommentController extends Controller
{

    public function store(CommentRequest $request)
    {
        $comment = Comment::insertRow($request);
        $comment['about_date'] = Carbon\Carbon::createFromTimeStamp(strtotime(date('Y-m-d H:i:s')))->diffForHumans() ;
        return response()->json(['comment' => $comment]);
    }

}
