<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Comment extends Model
{
    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [
        'content', 'article_id'
    ];


    /**
     * insert new comment row.
     */
    public static function insertRow($data)
    {
        $comment = new Comment();
        $comment->content = $data->content;
        $comment->article_id = $data->article_id;
        $comment->save();
        return $comment;
    }

    /**
     * update comment row.
     */
    public static function updateRow($data, $id)
    {
        $comment = Comment::findOrFail($id);
        $comment->content = $data->content;
        $comment->save();
        return $comment;
    }


    /**
     * every comment belong to an article.
     */
    public function comments()
    {
        return $this->belongsTo('App\Article');
    }

}
