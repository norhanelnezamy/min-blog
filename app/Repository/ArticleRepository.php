<?php

namespace App\Repository\Article;

use IlluminateDatabaseEloquentCollection;

interface ArticleRepository
{
    public function search(string $query = ""): Collection;
}
