<?php

namespace App\Repository\Article;

use App\Article;
use IlluminateDatabaseEloquentCollection;

class EloquentArticleRepository implements ArticleRepository
{
    public function search(string $query = ""): Collection
    {
        return Article::where('body', 'like', "%{$query}%")
            ->orWhere('title', 'like', "%{$query}%")
            ->get();
    }
}
