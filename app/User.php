<?php

namespace App;

use Illuminate\Notifications\Notifiable;
use Illuminate\Foundation\Auth\User as Authenticatable;

class User extends Authenticatable
{
    use Notifiable;

    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [
        'name', 'email', 'password', 'type'
    ];

    /**
     * The attributes that should be hidden for arrays.
     *
     * @var array
     */
    protected $hidden = [
        'password', 'remember_token',
    ];

    /**
     * insert new user row.
     */
    public static function insertRow($data)
    {
        $user = new User();
        $user->name = $data->name;
        $user->email = $data->email;
        $user->type = $data->type;
        $user->password = bcrypt($data->password);
        $user->save();
    }

    /**
     * update user row.
     */
    public static function updateRow($data, $id)
    {
        $user = User::findOrFail($id);
        $user->name = $data->name;
        $user->email = $data->email;
        $user->type = $data->type;
        if (!empty($data->password)) {
            $user->password = bcrypt($data->password);
        }
        $user->save();
    }
}
