<?php

namespace App\Providers;

use Illuminate\Support\ServiceProvider;
use App\Repository\Article\EloquentArticleRepository;
use App\Repository\Article\ArticleRepository;

class AppServiceProvider extends ServiceProvider
{
    /**
     * Bootstrap any application services.
     *
     * @return void
     */
    public function boot()
    {
        //
    }

    /**
     * Register any application services.
     *
     * @return void
     */
    public function register()
    {
      $this->app->bind(ArticleRepository::class, function(){
          return new EloquentArticlesRepository();
      });
    }
}
