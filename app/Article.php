<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Support\Facades\Storage;

class Article extends Model
{
    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [
        'title', 'content', 'photos', 'category_id'
    ];

    /**
     * The attributes that should be cast to native types.
     *
     * @var array
     */
    protected $casts = [
        'photos' => 'array',
        'tags' => 'json',
    ];

    /**
     * insert new article row.
     */
    public static function insertRow($data)
    {
        $article = new Article();
        $article->title = $data->title;
        $article->category_id = $data->category_id;
        $article->content = $data->content;
        $photos = [];
        if (!empty($data->photos)){
        foreach ($data->photos as $key => $photo){
            $photos[] = Storage::put('public/photos', $photo);
        }

        }
        $article->photos = $photos;
        $article->save();
    }

    /**
     * update article row.
     */
    public static function updateRow($data, $id)
    {
        $article = Article::findOrFail($id);
        $article->title = $data->title;
        $article->category_id = $data->category_id;
        $article->content = $data->content;
        if (!empty($data->photos)){

        $photos = $article->photos;
        foreach ($data['photos'] as $key => $photo){
            $photos[] = Storage::put('public/photos', $photo);
        }
        $article->photos = $photos;
        }
        $article->save();
    }

    public static function deletePhoto($article_id, $photo_id)
    {
        $article = Article::findOrFail($article_id);
        $photos = $article->photos;
        if (Storage::exists($photos[$photo_id])) {
            Storage::delete($photos[$photo_id]);
            unset($photos[$photo_id]);
            $article->photos = $photos;
            $article->save();
            return true;
        }
    }


    /**
     * every article has many visitors comments.
     */
    public function comments()
    {
        return $this->hasMany('App\Comment');
    }

    /**
     * every article belong to a category.
     */
    public function category()
    {
        return $this->belongsTo('App\Category');
    }

}
