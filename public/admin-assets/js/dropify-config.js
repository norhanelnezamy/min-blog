$(document).ready(function () {

    $('.dropify').dropify({
        messages: {
            'default': translate['translate.drag_drop_add'],
            'replace': translate['translate.drag_drop_replace'],
            'remove': translate['translate.remove'],
            'error': 'Ooops, something wrong appended.'
        },
        error: {
            'fileSize': 'The file size is too big (1M max).'
        }
    });

    $('.additional-photos-btn').on('click' , function (e) {
        e.preventDefault();
        let content = $('.additional-photos-hidden').html();
        $('.additional-photos').append("<div class='newlly'>"+content+"</div>");
        $('.newlly').find('.file-input-hidden').addClass('dropify');
        $('.dropify').dropify({
            messages: {
                'default': translate['translate.drag_drop_add'],
                'replace': translate['translate.drag_drop_replace'],
                'remove': translate['translate.remove'],
                'error': 'Ooops, something wrong appended.'
            },
            error: {
                'fileSize': 'The file size is too big (1M max).'
            }
        });
    });

});