# Simple Application

## Installation and configuration steps

```bash
git clone https://gitlab.com/norhanelnezamy/min-blog.git
```
```bash
composer update
```
```bash
php artisan key:generate
```
please create a database in your SQL serve

```bash
cp -i .env.example .env
```
configure your database connection

```bash
php artisan migrate
```
```bash
php artisan db:seed
```
```bash
php artisan serve
```

## Usage

Use [Landing](http://127.0.0.1:8000) to landing page.
Use [Dashboard-Login](http://127.0.0.1:8000/login) to login to our dashboard with credentials:

email: admin@admin.com

password: password
