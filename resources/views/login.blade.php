<!DOCTYPE html>
<html>
<head>
  <meta charset="utf-8">
  <meta name="viewport" content="width=device-width, initial-scale=1.0">
  <meta name="description" content="">
  <meta name="author" content="">

  <!-- App Favicon -->
  <link rel="shortcut icon" href="{{url('admin-assets/images/favicon.ico')}}">

  <!-- App title -->
  <title>Login</title>

  <!-- App CSS -->
  <link href="{{url('admin-assets/ltr/vertical/css/bootstrap.min.css')}}" rel="stylesheet" type="text/css" />
  <link href="{{url('admin-assets/ltr/vertical/css/core.css')}}" rel="stylesheet" type="text/css" />
  <link href="{{url('admin-assets/ltr/vertical/css/components.css')}}" rel="stylesheet" type="text/css" />
  <link href="{{url('admin-assets/ltr/vertical/css/icons.css')}}" rel="stylesheet" type="text/css" />
  <link href="{{url('admin-assets/ltr/vertical/css/pages.css')}}" rel="stylesheet" type="text/css" />
  <link href="{{url('admin-assets/ltr/vertical/css/menu.css')}}" rel="stylesheet" type="text/css" />
  <link href="{{url('admin-assets/ltr/vertical/css/responsive.css')}}" rel="stylesheet" type="text/css" />

</head>
<body>

  {{-- <div class="account-pages"></div> --}}
  <div class="clearfix"></div>
  <div class="wrapper-page">
    <div class="text-center">
      <a href="{{url('/')}}" class="logo"><span>Blog<span>Dashboard</span></span></a>
    </div>
    <div class="m-t-40 card-box">
      <div class="text-center">
        <h4 class="text-uppercase font-bold m-b-0">Login To Dashboard</h4>
      </div>
      <div class="panel-body">
        <form class="form-horizontal m-t-20" action="{{url('login')}}" method="post">
          {{csrf_field()}}
          <div class="form-group ">
            <div class="col-xs-12">
              <input class="form-control" type="text" required="" placeholder="Username" name="email" value="{{old('email')}}">
              @if ($errors->has('email'))
                <p class="color-red">{{ $errors->first('email') }}</p>
              @endif
            </div>
          </div>

          <div class="form-group">
            <div class="col-xs-12">
              <input class="form-control" type="password" required="" placeholder="Password" name="password">
              @if ($errors->has('password'))
                <p class="color-red">{{ $errors->first('password') }}</p>
              @endif
            </div>
          </div>

          <div class="form-group text-center m-t-30">
            <div class="col-xs-12">
              <button class="btn btn-custom btn-bordred btn-block waves-effect waves-light" type="submit">Login</button>
            </div>
          </div>

          <div class="form-group m-t-30 m-b-0">
            <div class="col-sm-12">
              @if (session()->has('msg'))
                <p class="color-red">{{ session()->get('msg') }}</p>
              @endif
              <a href="page-recoverpw.html" class="text-muted"><i class="fa fa-lock m-r-5"></i> Forgot your password?</a>
            </div>
          </div>

        </form>

      </div>
    </div>
    <!-- end card-box-->

  </div>
  <!-- end wrapper page -->
</body>
</html>
