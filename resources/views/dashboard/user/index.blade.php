@extends('dashboard.layout.app')
@section('page_title')
  <li><h4 class="page-title">@lang('translate.users')</h4></li>
@endsection
@section('content')
  <div class="row">
    <div class="col-sm-12">
      <div class="card-box table-responsive">
        <div class="dropdown pull-right">
          <a href="#" class="dropdown-toggle card-drop" data-toggle="dropdown" aria-expanded="false">
            <i class="zmdi zmdi-more-vert"></i>
          </a>
          <ul class="dropdown-menu" role="menu">
            <li class="divider"></li>
          </ul>
        </div>
        <h4 class="header-title m-t-0 m-b-30">@lang('translate.list_users')</h4>

        <div id="datatable-responsive_wrapper" class="dataTables_wrapper form-inline dt-bootstrap no-footer">
          <table id="datatable-buttons" class="table table-striped table-bordered dt-responsive nowrap" cellspacing="0" width="100%">
            <thead>
              <tr>
                  <th>@lang('translate.id')</th>
                  <th>@lang('translate.name')</th>
                  <th>@lang('translate.email')</th>
                  <th>@lang('translate.type')</th>
                  <th>@lang('translate.created_at')</th>
                  <th class="no-sort"  aria-label="Actions">@lang('translate.actions')</th>
              </tr>
            </thead>
            <tbody>
              @foreach ($users as $key => $user)
                <tr id="#user-{{ $user->id }}">
                    <td>{{$user->id}}</td>
                    <td>
                      <a href="{{ url('#dashboard/user/'.$user->id) }}" class="text-primary text-uppercase m-b-20 font-13">{{$user->name}}</a>
                    </td>
                    <td>{{$user->email}}</td>
                    <td>{{$user->type}}</td>
                    <td>
                      {{ Carbon\Carbon::parse($user->created_at)->format('D, d M Y H:i a') }}
                    </td>
                  <td class="actions">
                      <a href="{{ url('dashboard/user/'.$user->id.'/edit') }}" class=""><i class="fa fa-pencil"></i></a>
                      <a href="#" data-row-id="user-{{ $user->id }}" data-msg-key="delete_user_msg" data-url="{{ url('dashboard/user/'.$user->id) }}" onclick="deleteRow($(this))" class="text-danger"><i class="fa fa-trash-o"></i></a>
                  </td>
                </tr>
              @endforeach
            </tbody>
            {{$users->links()}}
          </table>
        </div>
      </div>
    </div><!-- end col -->
  </div>
  <!-- end row -->
@endsection
