@extends('dashboard.layout.app')
@section('page_title')
  <li><h4 class="page-title">@lang('translate.users')</h4></li>
@endsection
@section('content')
  <div class="card-box">
    <h4 class="header-title m-t-0 m-b-30">@lang('translate.insert_user')</h4>
    <div class="row">
      <div class="col-lg-10">
        <form class="form-horizontal" role="form" method="post" action="{{ url('dashboard/user') }}">
          {{ csrf_field() }}
          <div class="form-group">
            <label class="col-md-2 control-label">@lang('translate.name')</label>
            <div class="col-md-10">
              <input type="text" class="form-control" value="{{ old('name') }}" name="name" placeholder="@lang('translate.full_name')">
              @if ($errors->has('name'))
                <p class="color-red">{{ $errors->first('name') }}</p>
              @endif
            </div>
          </div>
          <div class="form-group">
            <label class="col-md-2 control-label" for="example-email">@lang('translate.email')</label>
            <div class="col-md-10">
              <input type="email" id="email" class="form-control" value="{{ old('email') }}" name="email" placeholder="@lang('translate_email')">
              @if ($errors->has('email'))
                <p class="color-red">{{ $errors->first('email') }}</p>
              @endif
            </div>
          </div>
          <div class="form-group">
            <label class="col-md-2 control-label">@lang('translate.password')</label>
            <div class="col-md-10">
              <input type="password" class="form-control" name="password" placeholder="********" value="{{old('password')}}">
              @if ($errors->has('password'))
                <p class="color-red">{{ $errors->first('password') }}</p>
              @endif
            </div>
          </div>
          <div class="form-group">
            <label class="col-md-2 control-label">@lang('translate.user_type')</label>
            <div class="col-md-10">
              <div class="radio radio-primary">
                <input type="radio" name="type" id="admin" value="admin">
                <label for="admin">
                  @lang('translate.type_admin')
                </label>
              </div>
              <div class="radio radio-primary">
                <input type="radio" name="type" id="else" value="else">
                <label for="else">
                  @lang('translate.type_else')
                </label>
              </div>
              @if ($errors->has('type'))
                <p class="color-red">{{ $errors->first('type') }}</p>
              @endif
            </div>
          </div>

          <div class="form-group">
            <button type="submit" class="btn btn-info waves-effect w-md waves-light m-b-5 pull-right">@lang('translate.save')</button>
          </div>
        </form>
      </div><!-- end col -->
    </div><!-- end row -->
  </div>
@endsection
@section('js')
  <script>
  $('input[name="type"][value={{old("type", !empty($type)? $type:"admin" )}}]').prop('checked', true);
  </script>
@endsection
