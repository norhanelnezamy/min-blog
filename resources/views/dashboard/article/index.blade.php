@extends('dashboard.layout.app')
@section('page_title')
  <li><h4 class="page-title">@lang('translate.articles')</h4></li>
@endsection
@section('content')
  <div class="row">
    <div class="col-sm-12">
      <div class="card-box table-responsive">
        <div class="dropdown pull-right">
          <a href="#" class="dropdown-toggle card-drop" data-toggle="dropdown" aria-expanded="false">
            <i class="zmdi zmdi-more-vert"></i>
          </a>
          <ul class="dropdown-menu" role="menu">
            <li><a href="#">Action</a></li>
            <li><a href="#">Another action</a></li>
            <li><a href="#">Something else here</a></li>
            <li class="divider"></li>
            <li><a href="#">Separated link</a></li>
          </ul>
        </div>
        <h4 class="header-title m-t-0 m-b-30">@lang('translate.list_articles')</h4>
        <table id="datatable-responsive" class="table table-striped table-bordered dt-responsive nowrap" cellspacing="0" width="100%">
          <thead>
            <tr>
              <th>@lang('translate.id')</th>
              <th>@lang('translate.title')</th>
              <th>@lang('translate.content')</th>
              <th class="sorting_disabled"  aria-label="Actions">@lang('translate.actions')</th>
            </tr>
          </thead>
          <tbody>
            @foreach ($articles as $key => $article)
              <tr>
                <td>{{$article->id}}</td>
                <td>{{$article->title}}</td>
                <td>
                  <div class="limit-text">
                  <?php echo $article->content;?>
                  </div>
                </td>
                <td class="actions">
                  <a href="{{ url('dashboard/article/'.$article->id.'/edit') }}" class=""><i class="fa fa-pencil"></i></a>
                  <a href="#" data-row-id="article-{{ $article->id }}" data-msg-key="delete_article_msg" data-url="{{ url('dashboard/article/'.$article->id) }}" onclick="deleteRow($(this))" class="text-danger"><i class="fa fa-trash-o"></i></a>
                </td>
              </tr>
            @endforeach
          </tbody>
          {{$articles->links()}}
        </table>
      </div>
    </div><!-- end col -->
  </div>
  <!-- end row -->
@endsection
