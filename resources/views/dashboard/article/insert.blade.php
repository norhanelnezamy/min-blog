@extends('dashboard.layout.app')
@section('page_title')
  <li><h4 class="page-title">@lang('translate.articles')</h4></li>
@endsection
@section('content')
  <div class="card-box">
    <div class="dropdown pull-right">
      <a href="#" class="dropdown-toggle card-drop" data-toggle="dropdown" aria-expanded="false">
        <i class="zmdi zmdi-more-vert"></i>
      </a>
      <ul class="dropdown-menu" role="menu">
        <li><a href="#">Action</a></li>
        <li><a href="#">Another action</a></li>
        <li><a href="#">Something else here</a></li>
        <li class="divider"></li>
        <li><a href="#">Separated link</a></li>
      </ul>
    </div>
    <h4 class="header-title m-t-0 m-b-30">@lang('translate.insert_article')</h4>
    <div class="row">
      <div class="col-lg-12">
        <form class="form-horizontal" role="form" method="post" action="{{ url('dashboard/article') }}" enctype="multipart/form-data">
          {{ csrf_field() }}
          <div class="form-group">
            <label class="col-md-1 control-label">@lang('translate.title')</label>
            <div class="col-md-10">
              <input type="text" class="form-control" value="{{ old('title') }}" name="title">
              @if ($errors->has('title'))
                <p style="color:red">{{ $errors->first('title') }}</p>
              @endif
            </div>
          </div>
          <div class="form-group">
            <label class="col-md-1 control-label">@lang('translate.category')</label>
            <div class="col-md-10">
              <select name="category_id" class="form-control select2">
                @foreach($categories as $category)
                  <option value="{{$category->id}}">{{$category->name}}</option>
                @endforeach
              </select>
              @if ($errors->has('category_id'))
                <p style="color:red">{{ $errors->first('category_id') }}</p>
              @endif
            </div>
          </div>
          <div class="form-group">
            <label class="col-md-1 control-label">@lang('translate.content')</label>
            <div class="col-md-10">
              <textarea id="elm1" name="content">{{ old('content') }}</textarea>
              @if ($errors->has('content'))
                <p style="color:red">{{ $errors->first('content') }}</p>
              @endif
            </div>
          </div>
          <div class="form-group">
            <label class="col-md-1 control-label">@lang('translate.photos')</label>
            <div class="col-sm-10 additional-photos">
              <div class="col-md-3" style="margin-bottom: 3px">
                <input type="file" name="photos[]" class="dropify" data-default-file="" />
                @if ($errors->has('photos.*'))
                  <p style="color:red">: {{ $errors->first('photos.*') }}</p>
                @endif
              </div>
              <div class="col-md-3" style="margin-bottom: 3px">
                <input type="file" name="photos[]" class="dropify" data-default-file="" />
              </div>
              <div class="col-md-3" style="margin-bottom: 3px">
                <input type="file" name="photos[]" class="dropify" data-default-file="" />
              </div>
              <div class="col-md-3" style="margin-bottom: 3px">
                <input type="file" name="photos[]" class="dropify" data-default-file="" />
              </div>
            </div>
          </div>
          <div class="additional-photos-hidden" style="display: none">
            <div class="col-md-3" style="margin-bottom: 3px">
              <input type="file" name="photos[]" class="file-input-hidden" data-default-file=""  />
            </div>
          </div>
          <div class="form-group">
            <div class="col-md-1">
            </div>
            <div class="col-md-10">
              <div class="col-md-12">
                <button type="button" class="btn btn-icon waves-effect additional-photos-btn waves-light btn-success m-b-5"> <i class="fa fa-plus-circle"></i>  @lang('translate.add_more_photo')</button>
              </div>
            </div>
          </div>
          <div class="form-group">
            <button type="submit" class="btn btn-info waves-effect w-md waves-light m-b-5 pull-right">@lang('translate.save')</button>
          </div>

        </form>
      </div><!-- end col -->
    </div><!-- end row -->
  </div>
@endsection

@section('js')
  <!-- file uploads js -->
  <script src="{{ asset('admin-assets/ltr/vertical/plugins/fileuploads/js/dropify.min.js')}}"></script>
  <!-- Form wizard -->
  <script src="{{ url('admin-assets/ltr/vertical/plugins/tinymce/tinymce.min.js') }}"></script>

  <script src="{{ url('admin-assets/js/tinymce-config.js') }}"></script>
  <script src="{{ url('admin-assets/js/dropify-config.js') }}"></script>
@endsection