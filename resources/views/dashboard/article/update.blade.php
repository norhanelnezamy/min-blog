@extends('dashboard.layout.app')
@section('page_title')
  <li><h4 class="page-title">@lang('translate.articles')</h4></li>
@endsection
@section('content')
  {{-- @if ($errors->any())
    <ul>
      @foreach ($errors->all() as $error)
        <li>{{ $error }}</li>
      @endforeach
    </ul>
  @endif --}}
  <div class="card-box">
    <div class="dropdown pull-right">
      <a href="#" class="dropdown-toggle card-drop" data-toggle="dropdown" aria-expanded="false">
        <i class="zmdi zmdi-more-vert"></i>
      </a>
      <ul class="dropdown-menu" role="menu">
        <li><a href="#">Action</a></li>
        <li><a href="#">Another action</a></li>
        <li><a href="#">Something else here</a></li>
        <li class="divider"></li>
        <li><a href="#">Separated link</a></li>
      </ul>
    </div>
    <h4 class="header-title m-t-0 m-b-30">@lang('translate.update_article')</h4>
    <div class="row">
      <div class="col-lg-12">
        <form class="form-horizontal" role="form" method="post" action="{{url('dashboard/article/'.$article->id)}}" enctype="multipart/form-data">
          {{csrf_field()}}
          {{method_field('PUT')}}
          <div class="form-group">
            <label class="col-md-1 control-label">@lang('translate.title')</label>
            <div class="col-md-10">
              <input type="text" class="form-control" value="{{old('title', $article->title)}}" name="title">
              @if ($errors->has('title'))
                <p style="color:red">{{ $errors->first('title') }}</p>
              @endif
            </div>
          </div>
          <div class="form-group">
            <label class="col-md-1 control-label">@lang('translate.category')</label>
            <div class="col-md-10">
              <select name="category_id" class="form-control select2">
                @foreach($categories as $category)
                  <option value="{{$category->id}}" {{$category->id == $article->category_id?'selected':''}}>{{$category->name}}</option>
                @endforeach
              </select>
              @if ($errors->has('category_id'))
                <p style="color:red">{{ $errors->first('category_id') }}</p>
              @endif
            </div>
          </div>
          <div class="form-group">
            <label class="col-md-1 control-label">@lang('translate.content')</label>
            <div class="col-md-10">
              <textarea id="elm1" name="content"><?php echo old('content', $article->content);?></textarea>
              @if ($errors->has('content'))
                <p style="color:red">{{ $errors->first('content') }}</p>
              @endif
            </div>
          </div>

          <div class="form-group">
            <label class="col-sm-1 control-label">@lang('translate.photos')</label>
            <div class="col-sm-8 additional-photos">
              @foreach($article->photos as $key =>  $photo)
                <div class="col-md-3 parent" style="margin-bottom: 3px">
                  <div class="dropify-wrapper has-preview">
                    <button type="button" data-url="{{ url('dashboard/article/'.$article->id.'/delete/photo/'.$key)  }}" data-msg-key="delete_article_photo_msg" class="dropify-clear" onclick="deleteRow($(this))">Delete</button>
                    <div class="dropify-preview" style="display: block;">
                      <span class="dropify-render">
                        <img src="{{ Storage::url($photo)}}">
                      </span>
                      <div class="dropify-infos"></div>
                    </div>
                  </div>
                </div>
              @endforeach
            </div>
          </div>

          <div class="additional-photos-hidden" style="display: none">
            <div class="col-md-3" style="margin-bottom: 3px">
              <input type="file" name="photos[]" class="file-input-hidden" data-default-file=""  />
            </div>
          </div>
          <div class="form-group">
            <div class="col-md-2">

            </div>
            <div class="col-md-8">
              <div class="col-md-12">
                <button type="button" class="btn btn-icon waves-effect additional-photos-btn waves-light btn-success m-b-5"> <i class="fa fa-plus-circle"></i>  @lang('translate.add_more_photo')</button>
              </div>
            </div>
          </div>

          <div class="form-group">
            <button type="submit" class="btn btn-info waves-effect w-md waves-light m-b-5 pull-right">@lang('translate.save')</button>
          </div>

        </form>
      </div><!-- end col -->
    </div><!-- end row -->
  </div>
@endsection

@section('js')

  <script src="{{ asset('admin-assets/ltr/vertical/plugins/fileuploads/js/dropify.min.js')}}"></script>
  <!-- Form wizard -->
  <script src="{{ url('admin-assets/ltr/vertical/plugins/tinymce/tinymce.min.js') }}"></script>
  <script src="{{ url('admin-assets/js/tinymce-config.js') }}"></script>
  <script src="{{ url('admin-assets/js/dropify-config.js') }}"></script>
@endsection
