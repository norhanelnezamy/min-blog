<!DOCTYPE html>
<html>
<head>
  <meta charset="utf-8">
  <meta name="viewport" content="width=device-width, initial-scale=1.0">
  <meta name="author" content="">

  <!-- App Favicon -->
  <link rel="shortcut icon" href="{{url('admin-assets/ltr/vertical/images/favicon.ico')}}">

  <!-- App title -->
  <title>Laravel</title>

  <link href="{{ url('admin-assets/ltr/vertical/css/bootstrap.min.css') }}" rel="stylesheet" type="text/css" />

  <!-- Notification css (Toastr) -->
  <link href="{{url('admin-assets/ltr/vertical/plugins/toastr/toastr.min.css')}}" rel="stylesheet" type="text/css" />

  <!-- Sweet Alert css -->
  <link href="{{url('admin-assets/ltr/vertical/plugins/bootstrap-sweetalert/sweet-alert.css')}}" rel="stylesheet" type="text/css" />

  <!--venobox lightbox-->
  <link href="{{url('admin-assets/ltr/vertical/plugins/magnific-popup/dist/magnific-popup.css')}}"/>

  <link href="https://cdnjs.cloudflare.com/ajax/libs/select2/4.0.5/css/select2.min.css" rel="stylesheet" />

  <!-- form Uploads -->
  <link href="{{url('admin-assets/ltr/vertical/plugins/fileuploads/css/dropify.min.css')}}" rel="stylesheet" type="text/css" />

  <link href="{{url('admin-assets/ltr/vertical/css/core.css')}}" rel="stylesheet" type="text/css" />
  <link href="{{url('admin-assets/ltr/vertical/css/components.css')}}" rel="stylesheet" type="text/css" />
  <link href="{{url('admin-assets/ltr/vertical/css/icons.css')}}" rel="stylesheet" type="text/css" />
  <link href="{{url('admin-assets/ltr/vertical/css/pages.css')}}" rel="stylesheet" type="text/css" />
  <link href="{{url('admin-assets/ltr/vertical/css/menu.css')}}" rel="stylesheet" type="text/css" />
  <link href="{{url('admin-assets/ltr/vertical/css/responsive.css')}}" rel="stylesheet" type="text/css" />
  <link href="{{url('admin-assets/css/custom.css')}}" rel="stylesheet" type="text/css" />

  <!-- HTML5 Shiv and Respond.js IE8 support of HTML5 elements and media queries -->
  <!-- WARNING: Respond.js doesn't work if you view the page via file:// -->
  <!--[if lt IE 9]>
  <script src="https://oss.maxcdn.com/libs/html5shiv/3.7.0/html5shiv.js"></script>
  <script src="https://oss.maxcdn.com/libs/respond.js/1.3.0/respond.min.js"></script>
  <![endif]-->

  <script src="{{url('admin-assets/ltr/vertical/js/modernizr.min.js')}}"></script>

</head>

@php
  $auth_user = auth()->user();
@endphp
<body class="fixed-left">

  <!-- Begin page -->
  <div id="wrapper">

    <!-- Top Bar Start -->
    <div class="topbar">

      <!-- LOGO -->
      <div class="topbar-left">
        <a href="{{ url('dashboard') }}" class="logo"><span>Simple<span>Blog</span></span><i class="zmdi zmdi-layers"></i></a>
      </div>

      <!-- Button mobile view to collapse sidebar menu -->
      <div class="navbar navbar-default" role="navigation">
        <div class="container">

          <!-- Page title -->
          <ul class="nav navbar-nav navbar-left">
            <li>
              <button class="button-menu-mobile open-left">
                <i class="zmdi zmdi-menu"></i>
              </button>
            </li>
            @yield('page_title')
          </ul>

          <!-- Right(Notification and Searchbox -->
          <ul class="nav navbar-nav navbar-right">
            <li>
              <!-- Notification -->
              <div class="notification-box">
                <ul class="list-inline m-b-0">
                  <li>
                    <a href="#" class="right-bar-toggle">
                      <i class="zmdi zmdi-notifications-none"></i>
                    </a>
                    <div class="noti-dot">
                      <span class="dot"></span>
                      <span class="pulse"></span>
                    </div>
                  </li>
                </ul>
              </div>
              <!-- End Notification bar -->
            </li>
          </ul>

        </div><!-- end container -->
      </div><!-- end navbar -->
    </div>
    <!-- Top Bar End -->


    <!-- ========== Left Sidebar Start ========== -->
    <div class="left side-menu">
      <div class="sidebar-inner slimscrollleft">

        <!-- User -->
        <div class="user-box">
          <div class="user-img">
            <img src="{{url('index.png')}}" alt="user-img" title="{{$auth_user->name}}" class="img-circle img-thumbnail img-responsive">
            <div class="user-status online"><i class="zmdi zmdi-dot-circle"></i></div>
          </div>
          <h5><a href="#" class="capitalize">{{$auth_user->name}}</a> </h5>
          <ul class="list-inline">
            <li>
              <a href="{{url('dashboard/user/'.$auth_user->id.'/edit')}}" >
                <i class="zmdi zmdi-settings"></i>
              </a>
            </li>

            <li>
              <a href="{{url('dashboard/logout')}}" class="text-custom">
                <i class="zmdi zmdi-power"></i>
              </a>
            </li>
          </ul>
        </div>
        <!-- End User -->

        <!--- Sidemenu -->
        <div id="sidebar-menu">
          @include('dashboard.layout.sidebar')
          <div class="clearfix"></div>
        </div>
        <!-- Sidebar -->
        <div class="clearfix"></div>

      </div>

    </div>
    <!-- Left Sidebar End -->



    <!-- ============================================================== -->
    <!-- Start right Content here -->
    <!-- ============================================================== -->
    <div class="content-page">
      <!-- Start content -->
      <div class="content">
        @yield('content')
      </div> <!-- content -->

      <footer class="footer">
        2019 © <a href="https://example.com/">example.com</a>
      </footer>

    </div>


    <!-- ============================================================== -->
    <!-- End Right content here -->
    <!-- ============================================================== -->

  </div>

  @yield('modals')
  <!-- END wrapper -->

  <script>
    var resizefunc = [];
    var translate = @json(Lang::get('translate'));
  </script>

  <!-- jQuery  -->
  <script src="{{url('admin-assets/ltr/vertical/js/jquery.min.js')}}"></script>
  <script src="{{url('admin-assets/ltr/vertical/js/bootstrap.min.js') }}"></script>
  <script src="{{url('admin-assets/ltr/vertical/js/detect.js')}}"></script>
  <script src="{{url('admin-assets/ltr/vertical/js/fastclick.js')}}"></script>
  <script src="{{url('admin-assets/ltr/vertical/js/jquery.slimscroll.js')}}"></script>
  <script src="{{url('admin-assets/ltr/vertical/js/jquery.blockUI.js')}}"></script>
  <script src="{{url('admin-assets/ltr/vertical/js/waves.js')}}"></script>
  <script src="{{url('admin-assets/ltr/vertical/js/jquery.nicescroll.js')}}"></script>
  <script src="{{url('admin-assets/ltr/vertical/js/jquery.scrollTo.min.js')}}"></script>

  <!-- Toastr js -->
  <script src="{{url('admin-assets/ltr/vertical/plugins/toastr/toastr.min.js')}}"></script>

  <!-- Sweet Alert js -->
  <script src="{{url('admin-assets/ltr/vertical/plugins/bootstrap-sweetalert/sweet-alert.min.js')}}"></script>
  <script src="{{url('admin-assets/ltr/vertical/pages/jquery.sweet-alert.init.js')}}"></script>

  <!-- isotope filter plugin -->
  <script src="{{url('admin-assets/ltr/vertical/plugins/isotope/dist/isotope.pkgd.min.js')}}"></script>

  <script src="https://cdnjs.cloudflare.com/ajax/libs/select2/4.0.5/js/select2.min.js"></script>

  <!-- App js -->
  <script src="{{url('admin-assets/ltr/vertical/js/jquery.core.js')}}"></script>
  <script src="{{url('admin-assets/ltr/vertical/js/jquery.app.js')}}"></script>


  <script>
    // Select2
    $(".select2").select2();

    @if (session()->has('side_bar'))
    var sidebar = "{{ session()->get('side_bar') }}";
    $('.'+sidebar).addClass('active');

    var sidebar_tree = sidebar.split("_");
    $('a.'+sidebar_tree[0]).addClass('active subdrop');
    $('ul.'+sidebar_tree[0]).css('display', 'block');
    @endif

    @if (session()->has('msg'))
    toastr["info"]("@lang('translate.'.session()->get('msg'))");
    @endif

    function deleteRow(clickedBtn){
      swal({
        title: translate['delete_warning'],
        text: translate[clickedBtn.data('msg-key')],
        type: 'warning',
        showCancelButton: true,
        confirmButtonText: translate['continue'],
        cancelButtonText: translate['cancel'],
      }, function (isConfirm) {
        if (isConfirm) {
          var data = {
            _token : "{{ csrf_token() }}",
            _method : "DELETE"
          };

          sendPostAjaxRequest(clickedBtn.data("url"), data).then(function(msg){
            $(`#${clickedBtn.data('row-id')}`).remove();
            toastr["info"](msg);
          }).catch(function(msg){
            toastr["error"](msg);
          });
        }
      });
    }

    function sendPostAjaxRequest(url, data){
      return new Promise(function(resolved, rejected) {
        $.ajax({
          url:url,
          type: "POST",
          data: data,
          success: function (response) {
            if (response.status == 1) {
              resolved(translate[response.msg]);
            }else {
              rejected(translate[response.msg]);
            }
          },
          error: function (xhr, ajaxOptions, thrownError) {
            if (xhr.responseJSON.errors != undefined) {
              for (var error in xhr.responseJSON.errors) {
                $(`p.${error}`).text(xhr.responseJSON.errors[error][0]);
              }
            }else {
              rejected(translate['wrong_msg']);
            }
          }
        });
      });
    }
  </script>

  @yield('js')

</body>
</html>
