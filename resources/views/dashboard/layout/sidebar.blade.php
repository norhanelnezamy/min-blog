<ul>
  <li class="text-muted menu-title capitalize">@lang('translate.type'): {{ auth()->user()->type}}</li>

  <li>
    <a href="{{url('dashboard')}}" class="waves-effect index"><i class="zmdi zmdi-view-dashboard"></i> <span> @lang('translate.dashboard') </span> </a>
  </li>

    <li class="has_sub">
      <a href="javascript:void(0);" class="waves-effect user"><i class="fa fa-group"></i> <span> @lang('translate.users') </span> <span class="menu-arrow"></span></a>
      <ul class="list-unstyled user">
          <li class="user_index"><a href="{{url('dashboard/user')}}">@lang('translate.list_users')</a></li>
          <li class="user_create"><a href="{{url('dashboard/user/create')}}">@lang('translate.insert_user')</a></li>
      </ul>
    </li>

    <li>
        <a href="{{url('dashboard/category')}}" class="waves-effect category"><i class="fa fa-cubes"></i> <span> @lang('translate.categories') </span> </a>
    </li>

    <li class="has_sub">
      <a href="javascript:void(0);" class="waves-effect article"><i class="fa fa-file"></i> <span> @lang('translate.articles') </span> <span class="menu-arrow"></span></a>
      <ul class="list-unstyled article">
          <li class="article_index"><a href="{{url('dashboard/article')}}">@lang('translate.list_articles')</a></li>
          <li class="article_create"><a href="{{url('dashboard/article/create')}}">@lang('translate.insert_article')</a></li>
      </ul>
    </li>

</ul>
