@extends('dashboard.layout.app')
@section('page_title')
  <li><h4 class="page-title">@lang('translate.categories')</h4></li>
@endsection
@section('content')
  <div class="row">
    <div class="col-sm-12">
      <div class="card-box table-responsive">
        <div class="dropdown pull-right">
          <a href="#" class="dropdown-toggle card-drop" data-toggle="dropdown" aria-expanded="false">
            <i class="zmdi zmdi-more-vert"></i>
          </a>
          <ul class="dropdown-menu" role="menu">
            <li class="divider"></li>
          </ul>
        </div>
        <h4 class="header-title m-t-0 m-b-30">@lang('translate.list_categories')</h4>

        <div class="row">
          <div class="col-sm-6"></div>
          <div class="col-sm-6 text-right">
              <a href="#" data-toggle="modal" data-target=".category-modal" data-url="{{ url('dashboard/category') }}" class="btn btn-success btn-md waves-effect waves-light m-b-30 insert-row" data-animation="fadein" data-plugin="categorymodal" data-overlaySpeed="200" data-overlayColor="#36404a"><i class="md md-add"></i>@lang('translate.insert_category')</a>
          </div><!-- end col -->
        </div>
        <div id="datatable-responsive_wrapper" class="dataTables_wrapper form-inline dt-bootstrap no-footer">
          <table id="datatable-buttons" class="table table-striped table-bordered dt-responsive nowrap" cellspacing="0" width="100%">
            <thead>
              <tr>
                  <th>@lang('translate.id')</th>
                  <th>@lang('translate.name')</th>
                  <th>@lang('translate.created_at')</th>
                <th class="no-sort"  aria-label="Actions">@lang('translate.actions')</th>
              </tr>
            </thead>
            <tbody>
              @foreach ($categories as $key => $category)
                <tr id="#category-{{ $category->id }}">
                    <td>{{ $category->id }}</td>
                    <td>{{ $category->name }}</td>
                    <td>
                      {{ Carbon\Carbon::parse($category->created_at)->format('D, d M Y H:i a') }}
                    </td>
                  <td class="actions">
                      <a href="#" data-toggle="modal" data-target=".category-modal" data-category-id="{{ $category->id }}" data-url="{{ url('dashboard/category/'.$category->id) }}" class="update-row"><i class="fa fa-pencil"></i></a>
                      <a href="#" data-row-id="category-{{ $category->id }}" data-msg-key="delete_category_msg" data-url="{{ url('dashboard/category/'.$category->id) }}" onclick="deleteRow($(this))" class="text-danger"><i class="fa fa-trash-o"></i></a>
                  </td>
                </tr>
              @endforeach
            </tbody>
          </table>
        </div>
      </div>
    </div><!-- end col -->
  </div>
  <!-- end row -->
@endsection
@section('modals')
  <!-- Modal -->
  <div class="modal fade category-modal" tabindex="-1" role="dialog" aria-labelledby="mySmallModalLabel" aria-hidden="true" style="display: none;">
    <div class="modal-dialog modal-sm">
      <div class="modal-content">
        <div class="modal-header">
          <button type="button" class="close" data-dismiss="modal" aria-hidden="true">×</button>
          <h4 class="modal-title category-modal-title" id="mySmallModalLabel"></h4>
        </div>
        <div class="modal-body">
          <form class="form-horizontal category-form" role="form" method="post" action="">
            {{ csrf_field() }}
            <input type="hidden" name="url" value="{{ old('url') }}">
            <input type="hidden" name="_method" value="{{ old('_method') }}">
            <div class="form-group">
              <label class="col-md-2 control-label">@lang('translate.name')</label>
              <div class="col-md-10">
                <input type="text" class="form-control" value="{{ old('name') }}" name="name" placeholder="@lang('translate.enter_name')">
                @if ($errors->has('name'))
                  <p class="color-red">{{ $errors->first('name') }}</p>
                @endif
              </div>
            </div>
            <div class="form-group">
              <button type="submit" class="btn btn-info waves-effect w-md waves-light m-b-5 pull-right">@lang('translate.save')</button>
            </div>
          </form>
        </div>
      </div><!-- /.modal-content -->
    </div><!-- /.modal-dialog -->
  </div><!-- /.modal -->

@endsection
@section('js')
  <script>

    var categories =  @json($categories);
    $('.insert-row').on('click', function(){
      $('.category-modal-title').text(translate['insert_category']);
      $('.category-form').attr('action', $(this).data('url'));
      $('.category-form :input[name=url]').val($(this).data('url'));
      $('.category-modal').show('modal');
    });

    $('.update-row').on('click', function(){
      var category = categories.find(category => category.id == $(this).data('category-id'));
      $('.category-modal-title').text(translate['update_category']);
      $('.category-form').attr('action', $(this).data('url'));
      $('.category-form :input[name=url]').val($(this).data('url'));
      $('.category-form :input[name=name]').val(category.name);
      $('.category-form :input[name=_method]').val('PUT');
      $('.category-modal').show('modal');
    });
    @if ($errors->any())
      $('.category-modal-title').text(translate['{{  old("_method")== 'PUT'? 'update_category':'insert_category'}}']);
      $('.category-form').attr('action', '{{ old('url') }}');
      $('.category-modal').modal('show');
    @endif
  </script>
@endsection
