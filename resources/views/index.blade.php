<!DOCTYPE html>
<html>
<head>
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <!-- App Favicon -->
    <link rel="shortcut icon" href="{{url('admin-assets/ltr/horizontal/images/favicon.ico')}}">

    <!-- App title -->
    <title>Simple Blog</title>
    <link href="{{ url('admin-assets/ltr/horizontal/css/bootstrap.min.css') }}" rel="stylesheet" type="text/css" />

    <link href="{{url('admin-assets/ltr/horizontal/css/core.css')}}" rel="stylesheet" type="text/css" />
    <link href="{{url('admin-assets/ltr/horizontal/css/components.css')}}" rel="stylesheet" type="text/css" />
    <link href="{{url('admin-assets/ltr/horizontal/css/icons.css')}}" rel="stylesheet" type="text/css" />
    <link href="{{url('admin-assets/ltr/horizontal/css/pages.css')}}" rel="stylesheet" type="text/css" />
    <link href="{{url('admin-assets/ltr/horizontal/css/menu.css')}}" rel="stylesheet" type="text/css" />
    <link href="{{url('admin-assets/ltr/horizontal/css/responsive.css')}}" rel="stylesheet" type="text/css" />

    <link href="{{url('admin-assets/css/custom.css')}}" rel="stylesheet" type="text/css" />

</head>

<body>

<!-- Navigation Bar-->
<header id="topnav">
    <div class="topbar-main">
        <div class="container">
            <!-- LOGO -->
            <div class="topbar-left">
                <a href="index.html" class="logo"><span>Simple<span>Blog</span></span></a>
            </div>

            <div class="menu-extras">

                <ul class="nav navbar-nav navbar-right pull-right">
                    @auth
                        <li>
                            <a href="{{url('dashboard')}}">Dashboard</a>
                        </li>
                        <li>
                            <a href="{{url('logout')}}">Logout</a>
                        </li>
                    @else
                        <li>
                            <a href="{{url('login')}}">Login</a>
                        </li>
                    @endauth
                </ul>

            </div>
        </div>
    </div>

    <div class="navbar-custom">
        <div class="container">
            <div id="navigation">
                <!-- Navigation Menu-->
                <ul class="navigation-menu">
                    @foreach($categories as $category)
                    <li>
                        <a href="{{url('articles/category/'.$category->id)}}"> <i class="zmdi zmdi-view-dashboard"></i><span> {{$category->name}} </span> </a>
                    </li>
                    @endforeach

                </ul>
                <!-- End navigation menu  -->
            </div>
        </div>
    </div>
</header>
<!-- End Navigation Bar-->

<!-- Begin page -->
<div class="wrapper">
    <div class="container">
        <!-- Page-Title -->
        <div class="row">
            <div class="">
                <div class="card-box">
                    @foreach($articles as $key => $article)
                        <div class="comment" id="article-{{$article->id}}">
                            <div class="comment-body">
                                <div class="comment-text">
                                    <div class="comment-header">
                                        <a href="#" title="">{{$article->title}}</a>
                                        <span>about {{Carbon\Carbon::createFromTimeStamp(strtotime($article->created_at))->diffForHumans() }}</span>
                                    </div>
                                    <?php echo $article->content?>
                                </div>

                                <div class="comment-text">
                                    <form method="post" class="form-inline">
                                        <input type="text" class="input-comment" placeholder="Post a new comment" name="comment" data-article="{{$article->id}}">
                                        <div class="pull-right">
                                            <a class="btn btn-sm btn-primary waves-effect waves-light add-comment" data-article-id="{{$article->id}}">Send</a>
                                        </div>
                                    </form>
                                </div>
                            </div>

                            @foreach($article->comments as $key => $comment)
                                <div class="comment">
                                    <div class="comment-body">
                                        <div class="comment-text">
                                            <div class="comment-header">
                                                <a href="#" title=""># {{$comment->id}}</a>
                                                <span>about {{Carbon\Carbon::createFromTimeStamp(strtotime($comment->created_at))->diffForHumans() }}</span>
                                            </div>
                                            {{$comment->content}}
                                        </div>
                                    </div>
                                </div>
                            @endforeach
                        </div>
                    @endforeach

                </div>
            </div>
        </div>

    </div>
        <!-- end container -->
</div>

<!-- END wrapper -->

<!-- jQuery  -->
<script src="{{url('admin-assets/ltr/horizontal/js/jquery.min.js')}}"></script>
<script src="{{url('admin-assets/ltr/horizontal/js/bootstrap.min.js') }}"></script>
<script src="{{url('admin-assets/ltr/horizontal/js/detect.js')}}"></script>
<script src="{{url('admin-assets/ltr/horizontal/js/fastclick.js')}}"></script>
<script src="{{url('admin-assets/ltr/horizontal/js/jquery.slimscroll.js')}}"></script>
<script src="{{url('admin-assets/ltr/horizontal/js/jquery.blockUI.js')}}"></script>
<script src="{{url('admin-assets/ltr/horizontal/js/waves.js')}}"></script>
<script src="{{url('admin-assets/ltr/horizontal/js/jquery.nicescroll.js')}}"></script>
<script src="{{url('admin-assets/ltr/horizontal/js/jquery.scrollTo.min.js')}}"></script>
<script src="{{url('admin-assets/ltr/horizontal/js/wow.min.js')}}"></script>


<!-- App js -->
<script src="{{url('admin-assets/ltr/horizontal/js/jquery.core.js')}}"></script>
<script src="{{url('admin-assets/ltr/horizontal/js/jquery.app.js')}}"></script>

<script>

    $('a.add-comment').on('click', function(){
        var article_id = $(this).data('article-id');
        if ($(`input[name="comment"][data-article="${article_id}"]`).val() == "") {
            return false;
        }
        var data = {
            _token: "{{csrf_token()}}",
            content: $('input[name=comment]').val(),
            article_id: article_id
        };
        sendPostAjaxRequest("{{url('comment')}}", data).then(function(data){
        $(`#article-${article_id}`).append(`
        <div class="comment">
            <div class="comment-body">
                <div class="comment-text">
                    <div class="comment-header">
                        <a href="#" title=""># ${data.comment.id}</a>
                        <span>about ${data.comment.about_date}</span>
                    </div>
                    ${data.comment.content}
                </div>
             </div>
        </div>`);
        $(`input[name="comment"][data-article="${article_id}"]`).val('');
        }).catch(function(msg){
            console.log(msg)
        });

    });


    function sendPostAjaxRequest(url, data){
        return new Promise(function(resolved, rejected) {
            $.ajax({
                url:url,
                type: "POST",
                data: data,
                success: function (response) {
                   resolved(response);
                },
                error: function (xhr, ajaxOptions, thrownError) {
                    if (xhr.responseJSON.errors != undefined) {
                        for (var error in xhr.responseJSON.errors) {
                            $(`p.${error}`).text(xhr.responseJSON.errors[error][0]);
                        }
                    }else {
                        rejected("@lang('translate.wrong_msg')");
                    }
                }
            });
        });
    }
</script>

</body>
</html>