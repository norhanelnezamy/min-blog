<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

use App\Repository\Article\ArticleRepository;


Route::get('/search', function (ArticlesRepository $repository) {
    $articles = $repository->search((string) request('q'));
    return $articles;
});

//Route::get('login', 'LoginController@getLogin')->name('login');
//Route::post('login', 'LoginController@postLogin');
//
//Route::get('/', 'IndexController@index');
//Route::get('articles/category/{id}', 'IndexController@show');
//Route::post('comment', 'CommentController@store');
