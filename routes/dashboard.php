<?php
/*
|--------------------------------------------------------------------------
| dashboard Routes
|--------------------------------------------------------------------------
*/


  Route::get('/', 'IndexController@index');
  Route::get('logout', 'LoginController@logout');

  Route::resource('article', 'ArticleController');
  Route::resource('category', 'CategoryController');

  Route::resource('user', 'UserController');
